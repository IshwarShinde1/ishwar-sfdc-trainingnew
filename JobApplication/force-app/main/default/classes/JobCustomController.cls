public class JobCustomController {
public Job__c Job {get; private set;}
    
    public JobCustomController(){
        Id id=ApexPages.currentPage().getParameters().get('id');
        Job = (id==null)? new Job__c():[select Name, Active__c, Certification_Required__c, Job_Type__c, Description__c, Expires_On__c, Hired_Applicants__c, Manager__c, Manager_assign__c, Name__c, Number_of_Positions__c, Qualification_Required__c, Required_Skills__c,Salary_Offered__c, Total_Applicants__c from Job__c where Id=:id];
    }
    
        public PageReference save()
        {
            try{
                upsert(Job);
            }
            catch(System.DMLException e){
                ApexPages.addMessages(e);
                return null;    
            }
            PageReference redirectSuccess=new ApexPages.StandardController(Job).view();
            return redirectSuccess;
        }
}