@IsTest
public class UpadteJobUnitTest {
    public static TestMethod void updateJobTest(){
        Job__c jobdata = new Job__c();
        jobdata.Manager_assign__c = '0035g000001ulY5';
        jobdata.Number_of_Positions__c = 1;
        jobdata.Active__c = true;
        
        insert jobdata;
        
        Candidate__c candidate = new Candidate__c();
        candidate.Name = 'Test';
        candidate.Job__c = jobdata.Id;
        candidate.Status__c	 = 'Hired';
        candidate.Expected_Salary__c = 10000;
        
        insert candidate;
        
        Test.startTest();
        Database.SaveResult result = Database.update(jobdata);
        Test.stopTest();
        
        System.assert(result.isSuccess());
        System.assertEquals(false, jobdata.Active__c);
        //System.ass
    }
    
}