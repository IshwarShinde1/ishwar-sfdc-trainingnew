public class SendEmail {
    @future(callout=true)
    public static void SendToCandidate(String Id,String Email){
        
        /*https://winsurtech.com/blog/salesforce-system-visualforceexception-getting-content-from-within-triggers-not-supported/#:~:text=Problem%3A%20System.,in%20the%20trigger%20of%20sobject.&text=Solution%3A%20To%20resolve%20this%20exception,apex%20class%20with%20future%20method.*/
        /*https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_email_sending_attachments.htm*/
        //Attach a PDF to a Record in Salesforce
        /*https://blog.jeffdouglas.com/2010/07/14/attach-a-pdf-to-a-record-in-salesforce/#:~:text=Salesforce.com%20makes%20it%20extremely,saves%20it%20to%20an%20Account.*/
        
        Messaging.SingleEmailMessage Singleemail = new Messaging.SingleEmailMessage(); 
        Attachment attach = new Attachment();
        
        // Reference the attachment page and pass in the account ID
        PageReference pdf =  Page.CandidateDetailPDF;
        pdf.getParameters().put('id',Id); 
        pdf.setRedirect(true);
        
        // Take the PDF content
        Blob b = pdf.getContent();
        
        // Create the email attachment
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        attach.Body=b;
        efa.setFileName('CandidateDetailPDF.pdf');
        efa.setBody(b);
    	attach.Name =efa.filename;
		attach.IsPrivate = false;
        attach.ParentId=Id;
        insert attach;
        fileAttachments.add(efa);
        
        String[] toAddresses = Email.split(':', 0);
        
        Singleemail.setSubject( 'Congrachulations!..' );
        Singleemail.setToAddresses( toAddresses );
        Singleemail.setPlainTextBody( 'You Hired' );
        
        Singleemail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        
        // Sends the email
        Messaging.SendEmailResult [] r = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {Singleemail});   
        
    }
}