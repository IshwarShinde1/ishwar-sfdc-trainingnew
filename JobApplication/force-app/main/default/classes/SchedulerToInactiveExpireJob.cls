global class SchedulerToInactiveExpireJob implements Schedulable {
    
    public static String CRON_EXP = '0 0 18 * * ? *';
    
    global void execute(SchedulableContext sc){
        List<Job__c> joblists = new List<Job__c>();
        joblists = [ SELECT id, Active__c, Expires_On__c FROM Job__c WHERE Active__c = true AND Expires_On__c < TODAY ] ;
		
        for (Job__c jobdata : joblists){
            if(jobdata.Expires_On__c < System.today()){
                jobdata.Active__c=false;
            }
        }
        update joblists;
    }
}