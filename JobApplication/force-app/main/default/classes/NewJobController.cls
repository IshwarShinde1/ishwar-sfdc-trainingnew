public class NewJobController {

   //public String CandidateNew { get; set; }
   public List<sObject> records{get; set;}
   public List<Candidate__c> candidateRecords{get; set;}
   public List<String> fields{get; set;}
   public List<String> candidateFields{get; set;}
   public List<Job__c> job{get; set;}
   
   public NewJobController(){
       Id id= ApexPages.currentPage().getParameters().get('id');
       
       records=[select Name, Job_Type__c,  Number_of_Positions__c, Qualification_Required__c, Required_Skills__c,
                Salary_Offered__c, Certification_Required__c from Job__c where Id=:id];
       
       fields= new List<String>{'Name', 'Job_Type__c',  'Number_of_Positions__c', 'Qualification_Required__c', 'Required_Skills__c',
                'Salary_Offered__c', 'Certification_Required__c'};
       
         job= [select Job_Type__c from Job__c where Id=:id ];
       
       candidateRecords=[select Salutation__c, First_Name__c, Last_Name__c, Full_Name__c, Email__c, Status__c, Job__c, Age__c, DOB__c, Application_Date__c, Country__c, State__c, Expected_Salary__c  from Candidate__c where Job__c=:job];
       
      
   }
}