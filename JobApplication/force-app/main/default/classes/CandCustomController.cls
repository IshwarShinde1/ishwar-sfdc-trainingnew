public class CandCustomController {
    Id id;
    public Candidate__c CandidateNew {get; private set;}
    public static Id jobId {get;set;}
    
    public CandCustomController(NewJobController controller){
        CandCustomControllerNew();
    }
    public void CandCustomControllerNew(){
        id=ApexPages.currentPage().getParameters().get('id');
        try{
            	CandidateNew = (id!=null)? new Candidate__c():[select Application_Date__c, Country__c, Salutation__c, First_Name__c, Last_Name__c, Full_Name__c, DOB__c, Job__c, Email__c, State__c, Expected_Salary__c, Status__c from Candidate__c where Job__c=:id];            
           }catch(Exception e){
           		System.debug(e);
           }
    }
    
    
    public PageReference save()
    {
        try{
            upsert(CandidateNew);
        }
        catch(System.DMLException e){
            ApexPages.addMessages(e);
            return null;    
        }
        //PageReference redirectSuccess=new ApexPages.StandardController(CandidateNew).view();
        PageReference redirectSuccess = new PageReference('/apex/NewViewPage?id='+id);
        redirectSuccess.setRedirect(true);
        return redirectSuccess;
    }
}