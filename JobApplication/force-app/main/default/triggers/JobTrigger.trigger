trigger JobTrigger on Job__c (before update, after delete) {  
    JobTriggerCustomSet__c jt = JobTriggerCustomSet__c.getInstance('00e5g000002UqE5');
    if(jt.JobTriggerActiveness__c){
        if(trigger.isUpdate){
            JobTriggerHandler.jobActiveness(Trigger.New);
            JobTriggerHandler.sentMail(Trigger.New);
        }
        if(trigger.isDelete){
            JobTriggerHandler.jobStatus(Trigger.old);
        }
    }
    
}