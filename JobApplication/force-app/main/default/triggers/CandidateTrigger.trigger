trigger CandidateTrigger on Candidate__c (before insert, after insert, after update, before update) {
    
    //Commented part Day-15 Task:2
    //CandidateTrigger__c ct = CandidateTrigger__c.getInstance('00e5g000002UqE5');
    
    //if(ct.ChechActive__c == true){
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            
            CandidateTriggersHandler.checkActive(trigger.new);
            CandidateTriggersHandler.checkSalary(trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert || trigger.isUpdate){
            if(!PreventRecursion.firstCall) {
                PreventRecursion.firstCall = true;
                CandidateTriggersHandler.addDate(trigger.new);
            }
            
            //Commented Day-18 task:2
            for(Candidate__c candidatedata : trigger.new){
                if(candidatedata.Status__c =='Hired'){
                    SendEmail.SendToCandidate(candidatedata.Id,candidatedata.Email__c);
                }
            }
        }
    }
    
    //}
    
}